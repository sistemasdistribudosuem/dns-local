package dnslocal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

public class DNSLocalTest {

	@Test
	public void testServidorDNSLocal() throws IOException {
		DNSLocal dnsLocal = new DNSLocal(8080);
		
		dnsLocal.iniciaServidor();
		
		String response = fazRequisicao("8080", "soma");
		
		Assert.assertEquals("Endereco nao encontrato", response);
	}
	
	public String fazRequisicao(String porta, String nomeServico) throws IOException{
		StringBuilder result = new StringBuilder();
		
		String urlStr = String.format("http://localhost:%s/pesquisa?nome=%s", porta, nomeServico);
		
	    URL url = new URL(urlStr);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setConnectTimeout(5000);
	    conn.setRequestMethod("GET");
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	       result.append(line);
	    }
	    rd.close();
	    String response = result.toString();
		return response;
	}
}
