package dnslocal;

import java.util.HashMap;
import java.util.Map;

public class RaizDNS {
	
	private static Map<String, Endereco> raizes = new HashMap<>();
	
	public static void addRaiz(String nome, Endereco endereco){
		if(raizes.get(nome) != null)
			raizes.remove(nome);
		
		raizes.put(nome, endereco);
	}
	
	public static Endereco getRaiz(String nome){
		return raizes.get(nome);
	}

}
