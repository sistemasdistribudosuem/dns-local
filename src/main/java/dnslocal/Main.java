package dnslocal;

import java.io.IOException;

public class Main {
	
	private static final String PORTA = "porta";

	public static void main(String[] args) throws IOException{
		validaParametros(args);
		registraRaizes(args);
		
		new DNSLocal(Integer.valueOf(System.getProperty(PORTA))).iniciaServidor();
	}

	private static void validaParametros(String[] args) {
		String porta = System.getProperty(PORTA);
		if(porta == null)
			throw new RuntimeException("Informe a porta com -Dporta");
		
		if(args.length == 0)
			throw new RuntimeException("Informe ao menos um DNS raiz no formato \"<nomeDNS> <host>:<porta>\"");
	}

	private static void registraRaizes(String[] args) {
		for(String s : args){
			String[] split = s.split(" ");
			String nome = split[0];
			
			String[] splitEndereco = split[1].split(":");
			
			Endereco endereco = new Endereco(splitEndereco[0], splitEndereco[1]);
			System.out.println("Registrando raiz");
			System.out.print(nome+" - ");
			System.out.println(endereco);
			
			RaizDNS.addRaiz(nome, endereco);
		}
	}

}
