package dnslocal;

import java.util.HashMap;
import java.util.Map;

public class TabelaRoteamento {
	
	private static Map<String, Endereco> tabela = new HashMap<>();
	
	public static void addEndereco(String nome, Endereco endereco){
		Endereco existente = tabela.get(nome);
		if(existente != null)
			tabela.remove(nome);
		
		tabela.put(nome, endereco);
	}

	public static Endereco pesquisa(String nome) {
		return tabela.get(nome);
	}

}
