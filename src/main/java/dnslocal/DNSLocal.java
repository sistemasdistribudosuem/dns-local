package dnslocal;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class DNSLocal {
	
	int porta;
	String ip;
	
	DNSLocal(int porta ){
		this.porta = porta;
		this.ip = "localhost";
	}
	
	public int getPorta() {
		return porta;
	}
	
	public String getIp() {
		return ip;
	}
	
	public void iniciaServidor() throws IOException{
		HttpServer server = HttpServer.create(new InetSocketAddress(getPorta()), 0);
        server.createContext("/pesquisa", new Pesquisador());
        server.start();
	}
}
