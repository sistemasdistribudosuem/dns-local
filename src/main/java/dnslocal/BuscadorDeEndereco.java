package dnslocal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BuscadorDeEndereco {
	
	
	String nome;
	
	public BuscadorDeEndereco(String nome){
		this.nome = nome;
	}
	
	public String busca() throws IOException{
		try{
		String[] partes = this.nome.split("\\.");
		
		String resposta = buscaNaRaiz(partes);
		System.out.println("Resposta da raiz: "+resposta);
		return buscaNosServidoresDNS(resposta);
		}
		catch (Exception e) {
			e.printStackTrace();
			return "Erro ao buscar endereco "+nome;
		}
	}

	private String buscaNosServidoresDNS(String busca) throws IOException {
		System.out.println("buscanco:" + busca);
		
		if(busca.contains(",")){
			
			String[] partes = busca.split(",");
			if(partes.length > 2){
				String endereco = partes[0];
				String nomeDns = partes[1];
				String nomeABuscar = partes[2];
				
				System.out.println(String.format("Buscando %s no DNS %s", nomeABuscar, nomeDns));
	
				String resposta = requisicao(endereco, nomeABuscar);
				return buscaNosServidoresDNS(resposta);
			}
			return partes[0];
		}
		
		return busca;
	}

	private String buscaNaRaiz(String[] partes) throws IOException {
		String raiz = getRaiz(partes);
		String nome = getNome(partes);
		
		Endereco endereco = RaizDNS.getRaiz(raiz);
		if(endereco == null)
			return "Endereco nao encontrato";
		
		return requisicao(endereco.ip+":"+endereco.porta, nome);
	}

	private String getRaiz(String[] partes){
		return partes[partes.length  - 1];
	}
	
	private String getNome(String[] partes) {
		List<String> list = new ArrayList<String>( Arrays.asList(partes));
		list.remove(partes.length - 1);
		
		return String.join(".", list);
	}
	
	private String requisicao(String endereco, String nome) throws IOException{
		StringBuilder result = new StringBuilder();
		
		String urlStr = String.format("http://%s/pesquisa?nome=%s", endereco, nome);
		
	    URL url = new URL(urlStr);
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod("GET");
	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String line;
	    while ((line = rd.readLine()) != null) {
	       result.append(line);
	    }
	    rd.close();
	    String response = result.toString();
	    return response;
	}

}
