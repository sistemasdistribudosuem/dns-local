package dnslocal;

import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class Pesquisador implements HttpHandler {
	
	String response;

	@Override
	public void handle(HttpExchange args) throws IOException {
		String query = args.getRequestURI().getQuery();
		String[] parametros = query.split("&");
		
		if(!isValidaParametros(parametros))
			return;
		
		String parametro = parametros[0];
		if(!isValidaParametro(parametro))
			return;
		
		String[] split = parametro.split("=");
		String nome = split[1];
		
		encontraEndereco(nome);
		
		addResponse(args);
	}

	private void encontraEndereco(String nome) throws IOException {
		System.out.println("Pesquisando: "+nome);
		Endereco endereco = TabelaRoteamento.pesquisa(nome);
		if(endereco != null){
			addResponse(nome, endereco);
			return;
		}
		
		this.response = buscaEnderecoNoSistemaDeNomeacao(nome);
		if(!this.response.contains(" ") && this.response.contains(":")){
			String[] split = this.response.split(":");
			String ip = split[0];
			String porta = split[1];
			TabelaRoteamento.addEndereco(nome, new Endereco(ip, porta));
		}
	}

	private String buscaEnderecoNoSistemaDeNomeacao(String nome) throws IOException {
		return new BuscadorDeEndereco(nome).busca();
	}
	
	

	private void addResponse(String nome, Endereco endereco) {
		this.response = endereco.ip+":"+endereco.porta;
	}

	private boolean isValidaParametro(String parametro) {
		String[] chaveValor = parametro.split("=");
		if(chaveValor.length != 2){
			this.response = "Verifique o parâmetro informado";
			return false;
		}
		
		return true;
	}

	private boolean isValidaParametros(String[] parametros) {
		if(parametros.length != 1){
			this.response = "Informe apenas o nome do serviço";
			return false;
		}
		return true;
		
	}

	private void addResponse(HttpExchange args) throws IOException {
		String response = this.response;
		System.out.println("Respondendo: "+response);
        args.sendResponseHeaders(200, response.length());
        OutputStream os = args.getResponseBody();
        os.write(response.getBytes());
        os.close();
	}

}
