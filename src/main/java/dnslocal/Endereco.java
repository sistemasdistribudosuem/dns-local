package dnslocal;

public class Endereco {
	
	String ip;
	String porta;
	
	Endereco(String ip, String porta){
		this.ip = ip;
		this.porta = porta;
	}
	
	@Override
	public String toString() {
		return String.format("%s:%s", ip, porta);
	}

}
